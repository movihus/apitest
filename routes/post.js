var bodyParser  = require('body-parser');
var Request 	= require('request');

/*
 * GET /post/:id to get an specific post
 */
function getPost(req, res) {
	var id = req.params.id;

	try {
		if (id) {
			Request('https://jsonplaceholder.typicode.com/posts/' + id, function(error, response, body) {
				try {
					body = JSON.parse(body);
				} catch(e) {
					res.json({
						success: false,
						message: 'API Response not valid'
					});
				}

				// post found
				if (body.id) {
					res.json({
						success: true,
						message: "Post found",
						data: body
					});
				} else {
					res.status(404).json({
						success: false,
						message: "Post not found"
					})
				}
			});			
		} else {
			res.status(400).json({
				success: false,
				message: 'Post Id required'
			});
		}
	} catch (err) {
		res.json({
			success: false,
			message: 'Error getting Post'
		});
	}
}

/*
 * PUT /post/:id to update a post
 */
function updatePost(req, res) {
	var id = req.params.id;
	var userId = req.body.userId;
	var title = req.body.title;
	var body = req.body.body;
	
	try {
		if (id && userId && title && body) {
			Request.put('https://jsonplaceholder.typicode.com/posts/' + id, {
				json: { 'userId': userId, 'title': title, 'body': body }
			},
			function(error, response, rbody) {
				// post updated
				if (rbody.id) {
					res.json({
						success: true,
						message: "Post updated"
					});
				} else {
					res.json({
						success: false,
						message: "Post not updated"
					})
				}
			});
		} else {
			res.status(405).json({
				success: false,
				message: 'Parameters incomplete'
			});
		}
	} catch (err) {
		res.json({
			success: false,
			message: 'Error updating Post'
		});
	}	
}

/*
 * DELETE /post/:id to delete a post
 */
function deletePost(req, res) {
	var id = req.params.id;

	try {
		if (id) {
			Request.delete('https://jsonplaceholder.typicode.com/posts/' + id,	function(error, response, body) {
				// jsonplaceholder always returns and empty object, I can't kwow if the deletion was completed
				res.json({
					success: true,
					message: "Post deleted"
				});					
			});
		} else {
			res.json({
				success: false,
				message: 'Post Id required'
			});
		}
	} catch (err) {
		res.json({
			success: false,
			message: 'Error deleting Post'
		});
	}

}

/*
 * GET /posts to get all posts
 */
function getPosts(req, res) {
	try {
		Request('https://jsonplaceholder.typicode.com/posts/', function(error, response, body) {
			try {
				body = JSON.parse(body);
			} catch(e) {
				res.json({
					success: false,
					message: 'API Response not valid'
				});
			}

			// posts found
			if (body.length) {
				res.json({
					success: true,
					message: "Post found",
					data: body
				});
			} else {
				res.json({
					success: false,
					message: "Post not found"
				})
			}
		});		
	} catch (err) {
		res.json({
			success: false,
			message: 'Error getting Post'
		});
	}
}

/*
 * POST /posts to create a post
 */
function createPost(req, res) {
	var userId = req.body.userId;
	var title = req.body.title;
	var body = req.body.body;

	try {
		if (userId && title && body) {
			Request.post('https://jsonplaceholder.typicode.com/posts', {
				json: { 'userId': userId, 'title': title, 'body': body }
			},
			function(error, response, body) {
				if (body.id) {
					res.json({
						success: true,
						message: 'Post created successfuly',
						data: body
					});
				} else {
					res.json({
						success: false,
						message: 'Post not created'
					});
				}
			});
		} else {
			res.status(405).json({
				success: false,
				message: 'Parameters incomplete'
			});
		}
	} catch (err) {
		res.json({
			success: false,
			message: 'Error creating Post'
		});	
	}

}

/*
 * GET /post/byUserId/:userId to get all posts of an specific user
 */
function getPostByUserId(req, res) {
	var userId = req.params.userId;

	try {
		if (userId) {
			Request('https://jsonplaceholder.typicode.com/posts?userId=' + userId, function(error, response, body) {
				try {
					body = JSON.parse(body);
				} catch(e) {
					res.json({
						success: false,
						message: 'API Response not valid'
					});
				}

				// if body is present, then user has posts or user exists
				if (body.length) {
					res.json({
						success: true,
						message: "Getting posts by userId successful",
						data: body
					});
				} else {
					res.json({
						success: false,
						message: "No result with userId"
					});
				}
			});
		} else {
			res.json({
				success: false,
				message: 'User Id required'
			});
		}
	} catch (err) {
		res.json({
			success: false,
			message: 'Error getting Posts by userId'
		});	
	}
}

// export all the functions
module.exports = { getPost, updatePost, deletePost, getPosts, createPost, getPostByUserId }