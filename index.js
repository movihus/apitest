let express 	= require('express');
let morgan  	= require('morgan');
let jwt    		= require('jsonwebtoken'); // used to create, sign, and verify tokens
let config 		= require('./config/default.json'); // get our config file
let bodyParser  = require('body-parser');
let post  		= require('./routes/post');
let app 		= express();

let port = process.env.PORT || 3000;

app.set('secret', config.secret); // secret

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//don't show the log when it is test
if(process.env.NODE_ENV !== 'test') {
    //use morgan to log at command line
    app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}


// get an instance of the router for api routes
var apiRoutes = express.Router(); 

// authentication
apiRoutes.post('/authenticate', function(req, res){
	// this should be stored in a database
	var storedUsername = 'obiwan';
	var storedPassword = 'kenobi';
	
	// incoming params
	var username = req.body.username;
	var pass = req.body.password;

	// check if provided credentials are correct
	if (username == storedUsername && pass == storedPassword) {
		
		// create the token
		var token = jwt.sign({			
				data: 'Important payload',
			}, 
			app.get('secret'), 
			{
				expiresIn: '10h'
			}
		);

		// return success response
		res.json({ success: true, message: 'Authentication successful', token: token });
	} else {
		// auth wrong, return fail
		res.json({ success: false, message: 'Authentication failed.'});
	}
});

// route middleware to verify a token
apiRoutes.use(function(req, res, next) {
	var token = req.body.token || req.query.token || req.headers['x-access-token'];

	if (token) {
		// check authentication
		jwt.verify(token, app.get('secret'), function(err, decoded){
			if (err) {
				return res.json({ success: false, message: 'Token incorrect. Failed to authenticate.'});
			} else {
				// do whatever with this data
				req.decoded = decoded;    
        		next();
			}
		});
	} else {
		// token is not correct, unauthorized
		return res.status(403).json({
			success: false,
			message: 'Token is required'
		});
	}
});

apiRoutes.route('/posts/:id')
	.get(post.getPost)
	.put(post.updatePost)
	.delete(post.deletePost);

apiRoutes.route('/posts')
	.get(post.getPosts)
	.post(post.createPost);

// getting posts by userID
apiRoutes.route('/posts/byUser/:userId')
	.get(post.getPostByUserId);

// apply routes to the application with prefix v1
app.use('/v1', apiRoutes);

// start the app
app.listen(port, function () {
  console.log('Example EDGE app listening on port ' + port + '!');
});

module.exports = app; // for testing